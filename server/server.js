'use strict';
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();
const port = process.env.PORT || 3000;
//Rutas
const api = require('./api/upload');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', express.static(path.join(__dirname, '../build')));


//Api
app.use('/api', api);




app.listen(port, () => {
  console.log(`Listening port: ${port} - ${moment().unix()}`);
});
