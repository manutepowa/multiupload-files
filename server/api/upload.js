'use strict';
const express = require('express');
const moment = require('moment');
const multer = require('multer');
const path = require('path');
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		// console.log(file,req.body)
		cb(null, path.join(__dirname, '../../src/assets/'))
	},
	filename: function (req, file, cb) {
		let extension = file.mimetype;
		extension = extension.substring(extension.indexOf("/")+1, extension.length);
	 	cb(null, `${file.fieldname}-${moment().unix()}.${extension}`);
  	}
});
const upload = multer({ storage: storage }).single('file');

const api = express();

api.get('/status', function(req, res, next) {
	console.log(path.join(__dirname))
	res.status(200).send({ msg: "Mensaje del servidor"});
});
/*
* Subida de archivos usando multer
*/
api.post('/upload', function (req, res, next) {
		upload(req, res, function (err) {
		    if (err) {
		      // An error occurred when uploading
		      return
		    }
			console.log(req.file,req.body)
		    res.status(200).send({ status: "ok"});
  		})
});




api.get('/get_data', function(req, res, next) {
	const data = {
		nombre: 'Manuel',
		edad: 27
	}
	res.status(200).send(data);
});


module.exports = api;
