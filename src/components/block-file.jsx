import React from 'react';
import axios from 'axios';


export default class BlockFile extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            progress: '0%',
            file: {},
            message: "Añade un archivo"
        }

        this.uploadFile = this.uploadFile.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }
    uploadFile(e){
        e.preventDefault();
        const reader = new FileReader();
        var data = new FormData();
        data.append('file',this.state.file);

        reader.readAsDataURL(this.state.file);

        reader.onloadstart = () => {
            console.log("Empiezo a subir");
            this.setState({
                message: "Subiendo!!!"
            })
        }
        reader.onprogress = (data) => {
            // console.log("subiendo.....",data);
            if (data.lengthComputable) {
                var progress = parseInt( ((data.loaded / data.total) * 100),10);
                // console.log("progreso: ",progress);
                this.setState({progress: `${progress}%`})
            }
        }
        reader.onloadend = () => {
            // this.setState({
            //     result: reader.result
            // });

            axios.post('/api/upload', data)
              .then(function (response) {
                    // console.log(response);
              })
              .catch(function (error) {
                    console.log(error);
              });
            console.log("Terminé");
            this.setState({message: "Finalizado con éxito"})
        }

        // this.setState({file: e.target.files[0]});
        // console.log(data.file)
    }
    onDrop(e){

        this.setState({
            file: e.target.files[0],
            message: "Archivo añadido!!"
        })
        console.log("en el cliente")
    }
    render(){
        return(
            <div>
                <div className="row">
                    <form onSubmit={this.uploadFile}>
                        <div className="col-xs-4">
                            <input
                                type="file"
                                accept="*/*"
                                onChange={this.onDrop}
                                />
                        </div>
                        <div className="col-xs-8 text-center">
                            <input
                                ref="bt"
                                type="submit"
                                className="btn"
                                value="Subir"/>
                            <h1 className="text-right">{this.state.message}</h1>
                        </div>
                    </form>
                </div>
	            <div className="row">
                    <div className="progress">
                        <div className="progress-bar"
                            role="progressbar"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{width: this.state.progress}}>
                          <span className="sr-only">70% Complete</span>
                        </div>
                  </div>
                </div>

            </div>
        )
    }
}
