import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

//Components
import BlockFile from './components/block-file.jsx';

import './styles/style.scss';
class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subiendo: true,
            serverMsg: ""
        }

        this.getMsgServer = this.getMsgServer.bind(this);
    }


    getMsgServer(){
        console.log("ENTOOOOO")
        axios.get('/api/status')
          .then((response) => {
              this.setState({
                  serverMsg: response.data.msg
              })
          })
          .catch((error) => {
                console.log(error);
          });
    }
    renderOperation(){
        return(
            <div className="row">
            <div className="text-center">
                <h2>Otra petición al servidor</h2><br/>
                <button className="btn" onClick={this.getMsgServer}>Seguir operando</button>
                <h1>{this.state.serverMsg}</h1>
            </div>
            </div>
        )
    }
    render() {
        return (
            <div className="container">

                <BlockFile
                    key="1"
                />

                <BlockFile
                    key="2"
                />

                



                {this.state.subiendo == false ? <div></div> : this.renderOperation()}



            </div>

        );
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
